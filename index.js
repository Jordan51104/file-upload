const express = require('express');
const multer = require('multer');
const exphbs = require('express-handlebars');
const path = require('path');
require('dotenv').config();

const app = express(); 
const port = process.env.PORT || 3000;

app.use(express.static('static')); // <-------------- IMPORTANT: ALLOWS VIEW OF FILES IN THIS DIRECTORY
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//IMPORTANT: ALLOWS VIEW OF FILES IN THIS DIRECTORY
//IMPORTANT: ALLOWS VIEW OF FILES IN THIS DIRECTORY
//IMPORTANT: ALLOWS VIEW OF FILES IN THIS DIRECTORY
//IMPORTANT: ALLOWS VIEW OF FILES IN THIS DIRECTORY
//IMPORTANT: ALLOWS VIEW OF FILES IN THIS DIRECTORY
//IMPORTANT: ALLOWS VIEW OF FILES IN THIS DIRECTORY
//IMPORTANT: ALLOWS VIEW OF FILES IN THIS DIRECTORY
//IMPORTANT: ALLOWS VIEW OF FILES IN THIS DIRECTORY
//IMPORTANT: ALLOWS VIEW OF FILES IN THIS DIRECTORY
//IMPORTANT: ALLOWS VIEW OF FILES IN THIS DIRECTORY
//IMPORTANT: ALLOWS VIEW OF FILES IN THIS DIRECTORY

//set storage engine
const storage = multer.diskStorage({
	destination: './static/uploads/',
	filename: function (req, file, callback) { callback(null, file.originalname + Date.now() + path.extname(file.originalname)); }
});

//init upload var
const upload = multer({
	storage: storage,
	fileFilter: function (req, file, callback) {
		checkFileType(file, callback);
	}
}).single('fileupload');


//check filetype
function checkFileType(file, callback) {
	//allowed extensions
	const filetypes = /jpeg|jpg|png|gif|svg/;

	//check curr file ext
	let extname = filetypes.test(path.extname(file.originalname).toLowerCase());

	//check mimetype
	let mimetype = filetypes.test(file.mimetype);

	if (mimetype && extname) {
		return callback(null, true);
	} else {
		return callback("File not an image (.png, .jp*g, .gif)");
	}
}

//set view engine
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.get('/', (req, res) => {
	res.render('index');
});

app.post('/upload', (req, res) => {
	upload(req, res, (err) => {
		if (err) {
			res.render('index', {
				error: err
			});
		} else {
			if (req.file == undefined) {
				res.render('index', {
					error: 'No file selected'
				});
			} else {
				res.render('index', {
					file: `uploads/${req.file.filename}`
				});
			}
		}
	});
});

app.listen(port, () => {
	console.log(`listening on ${port}`);
});
